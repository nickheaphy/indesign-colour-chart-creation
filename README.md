# Tools to create Spot and CMYK charts

Various tools to create colour charts in Indesign

## Background

While there are numerious test files containing Spot and CMYK patches, all are optimised to fit as many on the sheet as possible. When inkjet printing on very light stock, these very dense charts were creating jaming problems as the saturated sheets of paper passed through the engine.

These scripts were developed to allow the creation of charts with configurable gaps between the colours - allowing charts with low overall ink coverage.

* `create_CMYK_Chart.js` - creates a CMYK chart using the percentages contained in the file (defaults to 10% increments)

* `create_ECI_Chart.js` - creates a CMYK chart using the values from the ECI (http://www.eci.org) Visual Chart

* `create_SPOT_Chart.js` - create a SPOT chart (see below)

## Usage

These scripts were intended for a single use for a specific customer, so they are pretty rudimentary.

The `create_SPOT_Chart.js` included in this repository does not really do anything as it needs the `pmscolours` array populated. As Pantone&reg; don't publish a library with names and LAB values I have ommited this from the repository. You can generate this yourself using the instructions below.

### Configuration

There are a couple of variables that can be edited in the files to configure the size of the squares and the gap between the squares. In the case of the CMYK charts you can also set an ink limit (set this to 400 if you don't want any ink limiting) and in `create_CMYK_Chart.js` you can set the percentages of colours that will be used.

### Installation

Copy the scripts to the scripts folder of Indesign.

### Using the Scripts

In Indesign make a new document. The measurement units should be set to mm and facing pages should be turned off. Page size can be set to whatever is required. Run the script. Go and have a coffee (the scripts can take a long time (ie the `create_CMYK_Chart.js` with no ink limiting and 10% increments takes hours to run!))

## Create an array from a Named Colour Profile

Install Argyll CMS https://www.argyllcms.com/. Obtain a Named Colour Profile containing the Spot Library you want (ie https://www.efi.com/marketing/fiery-servers-and-software/downloads/pantone-library/)

`iccdump -v 3 Profile.icc > iccdump.txt`

Clean up to create JS Array

`python3 create_JSArray_from_dump.py iccdump.txt > Spot_list.txt`

Copy this array to the script `create_SPOT_Chart.js` (replacing the existing array)

## Todo

Implement a dialog in Indesign when script is run to allow the parameters to be changed at run time (ie size, gap, TAC, colour percentages)