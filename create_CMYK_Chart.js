// Indesign Script to create a set of squares of a specific CMYK colours

//set the start position
var x1start = 5;
var y1start = 5;
//set the square size
var squaresize = 9;
//set the gap
var gap = 10;
// set the TAC
var TAC = 80;
//script name
var scriptname = "create_CMYK_Chart.js";
var scriptversion = "v1.1";
//the colour table variations
//var vcolours = [0, 10, 20, 30, 40, 55, 70, 85, 100]
var vcolours = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

var myDocument = app.activeDocument;
//change to the page
var myPage = myDocument.pages.item(0);
// Find the page width and height
width = myPage.bounds[3] - myPage.bounds[1]
height = myPage.bounds[2] - myPage.bounds[0]

//change to the masterpage and add some notes
var masterPage = myDocument.masterSpreads.item("A-Master");
var masterText = scriptname + " " + scriptversion + " (Size: " + squaresize + " Gap: " + gap + " TAC: " + TAC + " Colour Percents Used: " + vcolours.toString() + ")"
var myTextFrame = masterPage.textFrames.add({ geometricBounds: [myPage.bounds[2]-5, myPage.bounds[1], myPage.bounds[2], myPage.bounds[3]-1], contents: masterText });
var myText = myTextFrame.parentStory.paragraphs.item(0);
myText.pointSize = 6;
myText.justification = Justification.RIGHT_ALIGN;

//loop through the colours
x1 = x1start
y1 = y1start
for (c = 0; c < vcolours.length; c++){
    for (m = 0; m < vcolours.length; m++) {
        for (y = 0; y < vcolours.length; y++) {
            for (k = 0; k < vcolours.length; k++) {
                // check we have not exceeded TAC
                if (vcolours[c]+vcolours[m]+vcolours[y]+vcolours[k] > TAC) {
                    continue
                }
                // add the colour
                var coltext = "C" + vcolours[c] + "\nM" + vcolours[m] + "\nY" + vcolours[y] + "\nK" + vcolours[k];
                try {
                    myDocument.colors.item(coltext).name;
                } catch (myError) {
                    myDocument.colors.add({ name: coltext, model: ColorModel.PROCESS, space: ColorSpace.CMYK, colorValue: [vcolours[c], vcolours[m], vcolours[y], vcolours[k]] });
                }
                //draw the box
                var myRectangle = myPage.rectangles.add({ geometricBounds: [y1, x1, y1 + squaresize, x1 + squaresize] });
                myRectangle.strokeWeight = 0;
                myRectangle.fillColor = app.activeDocument.swatches.item(coltext);;
                myRectangle.fillTint = 100;
                //draw the text
                
                var myTextFrame = myPage.textFrames.add({ geometricBounds: [y1+squaresize+1, x1, y1+squaresize*2+5, x1+squaresize], contents: coltext });
                var myText = myTextFrame.parentStory.paragraphs.item(0);
                myText.pointSize = 5;

                x1 = x1 + squaresize + gap;
                if (x1 > width - squaresize) {
                    x1 = x1start
                    y1 = y1 + squaresize*2 + gap
                    if (y1 > height - squaresize*2) {
                        var myPage = myDocument.pages.add();
                        x1 = x1start
                        y1 = y1start
                    }
                }
            }
        }
    }
}
