# Convert CSV to JSArray
from __future__ import print_function
import sys
import csv
import os.path

def main(csv_to_open):

    with open(csv_to_open, 'r', encoding='utf-8-sig') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        headerrow = next(reader)

        for row in reader:
            colour_text = "C" + row[1] + " M" + row[2] + " Y" + row[3] + " K" + row[4]
            print('["%s", %s, %s, %s, %s],' % (colour_text, row[1], row[2], row[3], row[4]))







# ------------------------------
if __name__ == "__main__":
    # Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the csv file on the commandline")
        exit()
    
    if sys.version_info[0] != 3:
        print("Sorry, needs Python 3 to handle Unicode CSV files")
        exit(1)

    # Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the csv file on the commandline")
        exit(1)
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the CSV file")
        exit(1)

    main(sys.argv[1])