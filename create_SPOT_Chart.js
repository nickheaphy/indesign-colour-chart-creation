// Indesign Script to create a set of square of a specific colour

var myDocument = app.activeDocument;
var myPage = myDocument.pages.item(0);
// Find the page width and height
width = myPage.bounds[3] - myPage.bounds[1]
height = myPage.bounds[2] - myPage.bounds[0]
//set the start position
var x1start = 5;
var y1start = 5;
//set the square size
var squaresize = 10
//set the gap
var gap = 13
//the colour table
var pmscolours = [
    ["Spot 1", 78.489583, 16.839844, 67.257812],
    ["Spot 2", 76.738664, 12.000000, 32.906250],
    ["Spot 3", 63.368566, 14.636719, 39.878906]
]
//loop through the colours
x1 = x1start
y1 = y1start
var numpms = pmscolours.length;
for (i = 0; i < numpms; i++){
    // add the colour
    try {
        myDocument.colors.item(pmscolours[i][0]).name;
    } catch (myError) {
        myDocument.colors.add({ name: pmscolours[i][0], model: ColorModel.spot, space: ColorSpace.LAB, colorValue: [pmscolours[i][1], pmscolours[i][2], pmscolours[i][3]] });
    }
    //draw the box
    var myRectangle = myPage.rectangles.add({ geometricBounds: [y1, x1, y1 + squaresize, x1 + squaresize] });
    myRectangle.strokeWeight = 0;
    myRectangle.fillColor = app.activeDocument.swatches.item(pmscolours[i][0]);
    myRectangle.fillTint = 100;
    //draw the text
    var myTextFrame = myPage.textFrames.add({ geometricBounds: [y1+squaresize+1, x1, y1+squaresize*2-1, x1+squaresize], contents: pmscolours[i][0].replace("Spot ","") });
    //myTextFrame.textFramePreferences.verticalJustification = VerticalJustification.CENTER_ALIGN;
    var myText = myTextFrame.parentStory.paragraphs.item(0);
    myText.pointSize = 6;

    x1 = x1 + squaresize + gap;
    if (x1 > width - squaresize) {
        x1 = x1start
        y1 = y1 + squaresize*2 + gap
        if (y1 > height - squaresize*2) {
            var myPage = myDocument.pages.add();
            x1 = x1start
            y1 = y1start
        }
    }
}
